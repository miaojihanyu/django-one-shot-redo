from django.urls import path
from todos import views
from todos.views import todolist

urlpatterns = [path("", views.todolist, name="todo_list_list")]
